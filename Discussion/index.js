console.log("Hello, World");

//JS Objects
//JS objects is a way to define a real world object with its own characteristics.
//It is also a way to organize data with context through the use of key-value pairs.

/*
	loyal,
	huggable,
	4 legs,
	furry,
	different colors,
	hyperactive,
	tail,
	breed,
*/

//Create a list, use an array
//let dog = ["loyal",4,"tail","Husky"];

//Describe something with characteristics, use object
let dog = {

	breed: "Husky",
	color: "White",
	legs: 4,
	isHuggable: true,
	isLoyal: true

}

/*
	Mini-Activity

	Create an object that describes your favorite movie/video game
	-title
	-publisher
	-year
	-price (video game)
	-director (movie)
	-isAvailable
	
	log your object in the console and send it in the hangouts.

*/

let videoGame = {

	title: "Suikoden II",
	publisher: "Konami",
	year: "1997",
	price: 150,
	isAvailable: true

}

console.log(videoGame);

//If [] are used to create arrays, what did you use to create your object?
//[] - array literals create arrays
//{} - object literals create object

//Objects are composed of key-value pairs. keys provide label to your values.
//key: value
//Each key-value pair together are called properties

//Accessing array items = arrName[index]
//Accessing Object properties = objectName.propertyName

console.log(videoGame.title);

//What if I want to access the publisher of our videoGame?
console.log(videoGame.publisher);

//Can we also update the properties of our object?
//object.propertyName = <newValue>
videoGame.price = 200;
console.log(videoGame);

/*
	Mini-activity

	Re-assign the values of the object

	Video Game:

	title - "Final Fantasy X",
	publisher - "Square Enix",
	year - "2001"
	
	Movie:

	title - "Ang Probinsyano"
	publisher - "FPJ Productions"
	year - "1972"

	log the object in the console

*/

videoGame.title = "Final Fantasy X";
videoGame.publisher = "Square Enix";
videoGame.year = "2001";

console.log(videoGame);

//Objects can not only have primitive values like strings,numbers or boolean
//It can also contain objects and arrays.

let course = {

	title: "Philosophy 101",
	description: "Learn the values of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr. Johnson","Mrs. Smith","Mr. Francis"]

};

console.log(course);
//Can we access the course's instructors array?
console.log(course.instructors);
//How can we access, Mrs. Smith, an instructor from our instructors array?
console.log(course.instructors[1]);
//Can we use the instructor array's array methods?
//How can we delete Mr.Francis from our instructors array?
course.instructors.pop();
console.log(course);
//objectName.propertyNameArr.method();

/*
	Mini-Activity

	1. Add a new instructor to our course named "Mr. McGee"
		-log the instructors array in the console.

	2. Determine if the course has an instructor called "Mr.Johnson"
		-use only an array method.
		-save the result of the array method in a variable.
		-log the variable in the console.
		-Output should be a boolean.

*/

course.instructors.push("Mr. McGee");
console.log(course.instructors);

let isAnInstructor = course.instructors.includes("Mr. Johnson");
console.log(isAnInstructor);

//Create a function to be able to add new instructors to our object
function addNewInstructor(instructor){

	//Mini-Activity
	/*
		Add an if-else statement which will determine if the instructor being added is already in the instructors array
			-You can use an array method that determines if an item is in the array
			-Add an if-else statement
				-IF the instructor is already in the instructors array, show a  console.log() message:
				"Instructor already added."
				-Else add the instructor into the instructors array and show a  console.log() message:
				"Thank you. Instructor added."

		Add two new instructors of the same name, take a screenshot of your console

	*/

	//true or false
	let isAdded = course.instructors.includes(instructor);

	// if(isAdded === true)
	// if(isAdded)
	// if(course.instructors.includes(instructor))

	if(isAdded){
		console.log("Instructor already added.");
	} else {
		course.instructors.push(instructor);
		console.log("Thank you. Instructor added.");
	}



}

addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Smith");
addNewInstructor("Mr. Smith");

console.log(course);

//We can also create/initialize an empty object and then add its properties afterwards

let instructor = {};
console.log(instructor);

//If you assign a value to a property that does not yet exist,
//You are able to add a new property in the object.
instructor.name = "James Johnson";
console.log(instructor);

/*
	Mini-Activity

	Populate the instructor object with properties/characteristics that define/describe our instructor:

	James Johnson is an instructor. He is 56 years old and his gender is male.
 	His department is Humanities and his current salary is 50000. He teaches Philosophy, Humanities and Logic.

 	log the instructor object in the console and send it in the hangouts.


 	name James Johnson
 	age 56
 	gender male
 	department Humanities
 	salary 50000
 	subject Philosophy, Humanities, Logic

*/

instructor.age = 56;
instructor.gender = "male";
instructor.department = "Humanities";
instructor.salary = 50000;
instructor.subjects = ["Philosophy","Humanities","Logic"];

console.log(instructor);


instructor.address = {

	street: "#1 Maginhawa St.",
	city: "Quezon City",
	country: "Philippines"

}

console.log(instructor);
//How will we access the street property of our instructor's address?
console.log(instructor.address.street);

//Create Objects Using a constructor function

//Create a reusable function to create objects whose structure and keys are the same. Think of creating a function that serves a blueprint for an object

function Superhero(name,superpower,powerLevel){

	//"this" keyword when added in a constructor function refers to the object that will be made by the function.
	/*
		{
			name: <valueOfParameterName>,
			superpower: <valueOfParameterSuperpower>,
			powerLevel: <valueOfParameterPowerLevel>
		}

	*/
	this.name = name;
	this.superpower = superpower;
	this.powerLevel = powerLevel;

}

//Create an object out of our Superhero constructor function
//new keyword is added to allow us to create a new object out of our function.
let superhero1 = new Superhero("Saitama","One Punch", 30000);
console.log(superhero1);

/*
	Mini-Activity

	Create a constructor function able to receive 3 arguments
		-It should be able to receive 2 strings and a number
		-Using "this" keyword assign properties:
			name,
			brand,
			price
		-assign the parameters as values to each property

	Create 2 new objects called laptop1 and laptop2 using our constructor

	This constructor should be able to create laptop objects.

	Log the 2 laptop objects in the console.

*/

function Laptop(name,brand,price){

	this.name = name;
	this.brand = brand;
	this.price = price;

}

let laptop1 = new Laptop("Inspiron 15","Dell",25000);
let laptop2 = new Laptop("Aurora","Alienware",120000);

console.log(laptop1);
console.log(laptop2);


//Object Methods
//Object methods are functions that are associated with an object.
//A function is a property of an object and that function belongs to the object.
//Methods are tasks that an object can perform or do.

//arrayName.method()

let person = {
	name: "Slim Shady",
	talk: function(){

		//methods are functions associated as a property of an object.
		//They are anonymous functions we can invoke using the property of the object.
		//"this" refers to the object where the method is associated
		//console.log(this);
		console.log("Hi! My name is, What? My name is who? " + this.name);

	}
}

let person2 = {
	name: "Dr. Dre",
	greet: function(friend){

		//greet() method should be able to receive a single object
		//console.log(friend);
		console.log("Good day, " + friend.name);

	}
}

person.talk();
person2.greet(person);

//Create a constructor with a built-in method

function Dog(name,breed){

/*	{
		name: <valueParameterName>,
		breed: <valueParameterBreed>
	}*/
	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log("Bark! bark, " + friend.name);
	}

}

let dog1 = new Dog("Rocky","Bulldog");
console.log(dog1);

dog1.greet(person);
dog1.greet(person2);

let dog2 = new Dog("Blackie","Rottweiler");
console.log(dog2);

