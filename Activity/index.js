let trainer = {
    name: "Ash Ketchup",
    age: 12,
    pokemon: ["pikachu", "charizard", "squirtle", "bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function talk() {
        console.log("Pikachu! I Choose You!");
    }
}
console.log(trainer);

console.log("Result of Dot Notation");
console.log(trainer.name);

console.log("Result of Square Bracket Notation");
console.log(trainer["pokemon"]);

trainer.talk();


function pokemon(Name, Level, Health, Attack) {
    this.name = Name;
    this.level = Level;
    this.health = 3 * Level;
    this.attack = 1.5 * Level;
    this.tackle = function tackle(y) {
        if (y.health <= 0) {
            console.log(y.name + " has Already Fainted.");
        } else {
            console.log(this.name + " tackled " + y.name);
            y.health = y.health - this.attack;
            console.log(y.name + " has " + y.health + "HP Remaining");
            if (y.health <= 0) {
                y.fainted(y);
            }
        }
    }
    this.fainted = function faint(y) {

        console.log(y.name + " has fainted!");
    }
}

let pikachu = new pokemon("Pikachu", "12");
console.log(pikachu);

let geodude = new pokemon("Geodude", "8");
console.log(geodude);

let mew = new pokemon("Mew", "100");
console.log(mew);

pikachu.tackle(geodude);
pikachu.tackle(geodude);
pikachu.tackle(geodude);